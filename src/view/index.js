import Header from './components/header';
import Content from './components/content';

const renderApp = (header, content) => {
  let markup = `<div class='app'>`;
      markup +=   header;
      markup +=   content;
      markup += `</div>`;

  return markup;
}

export const render = (el, state) => {
  el.innerHTML = renderApp(
    Header(state),
    Content(state),
  );
};

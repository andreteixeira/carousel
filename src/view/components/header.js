const Header = () => {
  const markup =
    `<nav class='header'>
      <div class='header__content'>
        <div class='branding__items'>
          <div class='logo'>
            <img class='logo__img' src='https://www.klm.com/ams/frontend/img/klm_head@2x.png' />
          </div>
          <div class='rda'>
            <img class='rda__img' src='https://www.klm.com/travel/br_br/images/rda_regular_2x_tcm581-466994.png' />
          </div>
          <div class='sky__team'>
            <img class='sky__team__img' src='https://www.klm.com/ams/frontend/img/klm_skyteam@2x.png' />
          </div>
        </div>
        <div class='account__items'>
          <a href='#' > Brazil </a>
        </div>
      </div>
    </nav>`;

  return markup;
};

export default Header;

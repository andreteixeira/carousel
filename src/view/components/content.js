import ReservationForm from './reservationForm';
import AdditionalContent from './aditionalContent';
import MobileMenu from './mobileMenu';
import Bullets from './bullets';

const Content = (state) => {
  let bannerStyle = '';
  if (state.carousel.length > 0) {
    const divisionRest = state.config.selectedBanner % state.carousel.length;
    const bannerIdx = divisionRest === 0 ? state.carousel.length - 1 : divisionRest - 1;
    const selectedBanner = state.carousel[bannerIdx];
    bannerStyle = `background-image: url('${selectedBanner.image_url}')`;
  }

  return `<div class="content">
            <div class="banner" style="${bannerStyle}">
              <div class="arrows">
                <div class="arrow left"></div>
              </div>
              ${ReservationForm(state)}
              <div class="space__between"></div>
              ${AdditionalContent(state)}
              ${Bullets(state)}
              <div class="arrows">
                <div class="arrow right"></div>
              </div>
            </div>
            ${MobileMenu(state)}
          </div>`;
};

export default Content;

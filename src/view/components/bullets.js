import Bullet from './bullet';

const Bullets = (state) => {
  const bullets = [];

  for (var banner of state.carousel) {
    bullets.push(`${Bullet(state, banner.id)}`)
  }

  return `<div class="bullets">
            ${bullets.join('')}
          </div>`;
};

export default Bullets;

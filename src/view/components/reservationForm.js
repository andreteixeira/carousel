const ReservationForm = () =>
  `<div class='reservation'> 
    <div class='reservation__title'>Planeje e reserve</div>
    <div class='reservation__form'>
      <div class='from form__row'>
        <div class='label'> De </div>
        <div class='text__input'> <i class="fa fa-plane"></i><input type='text' /> </div>
      </div>
      <div class='to form__row'>
        <div class='label'> Para </div>
        <div class='text__input'> <i class="fa fa-plane arrival"></i><input type='text' /> </div>
      </div>
      <div class='when form__row'>
        <div class='label'> Partida </div>
        <div class='text__input'> <i class="fa fa-calendar"></i> <input type='text' /> </div>
      </div>
      <div class='who form__row'>
        <div class='label'> Passageiros </div>
        <div class='text__input'> <i class="fa fa-users"></i> <input type='text' /> </div>
      </div>
      <div class='class form__row'>
        <div class='label'> Classe de viagem </div>
        <div class='text__input'> <i class="fa fa-credit-card"></i><input type='text' /> </div>
      </div>
      <div class='search__button form__row'>
        <button> Pesquisar <i class="fa fa-chevron-right" aria-hidden="true"></i> </button>
      </div>
    </div>
  </div>`;

export default ReservationForm;

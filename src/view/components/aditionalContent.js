const AdditionalContent = (state) => {
  const selectedBanner = (state.carousel.length > 0) ? state.carousel[state.config.selectedBanner-1] : {title: "KLM The best flights", price: 1200}

  return `
          <div class='additional__content'> 
            <div class='additional__content__title'> 
              ${selectedBanner.title}
            </div>
            <div class='additional__content__price'> 
              Eur ${selectedBanner.price},00
            </div>
            Naast uw KLM vliegtickets meteen een hotel en/of auto boeken voor een scherpe prijs? Dat kan met KLM Package Deals!
          </div>
  `;
}

export default AdditionalContent;

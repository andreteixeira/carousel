const MobileMenu = (state) => {

  return `<div class='mobile__menu'> 
            <div class='menu__row'> <i class="fa fa-plane"></i> &nbsp; Planeje e reserve </div>
            <div class='menu__row'> <i class="fa fa-ticket"></i> &nbsp; Minha viagem </div>
            <div class='menu__row'> <i class="fa fa-laptop"></i> &nbsp; Check-in </div>
            <div class='menu__row'> <i class="fa fa-headphones"></i> &nbsp; Contato </div>
          </div>`;
};

export default MobileMenu;

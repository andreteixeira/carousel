const Bullet = (state, bannerId) => {
  let bulletStyle = 'bullet';
  if (bannerId === state.config.selectedBanner) {
    bulletStyle = 'bullet active';
  }
  return `<div class='${bulletStyle}' id='${bannerId}'></div>`;
};

export default Bullet;

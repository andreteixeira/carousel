import { listen } from './lib/events';
import { rootReducer } from './reducers';
import { nextBanner, previousBanner, selectBanner } from './actions';

export const registerEventHandlers = () => {
  listen('click', '.arrow', event => {
    if (event.target.matches('.left')) {
      rootReducer.dispatch(previousBanner());
    } else if (event.target.matches('.right')) {
      rootReducer.dispatch(nextBanner());
    }
  });

  listen('click', '.bullet', event => {
    rootReducer.dispatch(selectBanner(event.target.id));
  });
};

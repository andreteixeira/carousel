import { Server } from 'http';
import serverConfig from './config';

const app = serverConfig.app();
const server = new Server(app);
const port = process.env.PORT || 3000;

// Development server configuration
if (process.env.NODE_ENV !== 'production') {
  const webpack = require('webpack');
  const webpackDevMiddleware = require('webpack-dev-middleware');
  const webpackHotMiddleware = require('webpack-hot-middleware');
  const config = require('../../webpack.config.js');
  const compiler = webpack(config);

  app.use(webpackHotMiddleware(compiler));
  app.use(webpackDevMiddleware(compiler, {
      noInfo: true,
      publicPath: config.output.publicPath,
  }));
}

app.get('/', (req, res) => {
  res.render('index');
});

//Server startup
server.listen(port, '0.0.0.0', err => {
  if (err) {
      return console.error(err);
  }
  console.info('Server running on port ' + port);
});

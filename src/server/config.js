import path from 'path';
import express from 'express';
import exphbs from 'express-handlebars';
import bodyParser from 'body-parser';

module.exports = {
  app: function () {
    const app = express();
    const publicPath = express.static(path.resolve(__dirname, '../../dist'));

    // Defining assets configuration
    app.use('/dist', publicPath);

    // Templates configuration
    app.set('views', path.resolve(__dirname, '../templates'));
    app.engine('handlebars', exphbs({ defaultLayout: 'main', layoutsDir: path.resolve(__dirname, '../templates/layouts') }));
    app.set('view engine', 'handlebars');

    // body-parser. to enable usage of req.body
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());

    return app;
  },
};

/**
 * Código do redux-thunk, extraído de:
 * https://github.com/gaearon/redux-thunk/blob/master/src/index.js
 */

const createThunkMiddleware = (extraArgument) => {
  return ({ dispatch, getState }) => next => action => {
    if (typeof action === 'function') {
      return action(dispatch, getState, extraArgument);
    }

    return next(action);
  };
}

const thunk = createThunkMiddleware();

export default thunk;

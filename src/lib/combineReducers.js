export const combineReducers = (reducers) => {
	const reducerKeys = Object.keys(reducers);
	const finalReducers = {};

	for (let i = 0; i < reducerKeys.length; i++) {
	  const key = reducerKeys[i];
	  if (typeof reducers[key] === 'function') {
	    finalReducers[key] = reducers[key]
	  };
	};

	return finalReducers;
}

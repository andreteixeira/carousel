import data from "../data/seed";

export const fetchFromSeed = () => {
	const seedPromise = new Promise((resolve, reject) => {
		resolve(JSON.stringify(data['CAROUSEL']));
	})

	return seedPromise;
}
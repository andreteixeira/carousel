export const createStore = (reducers, initial = {}, middleware) => {
  const listeners = [];
  let actions = []
  let state = initial;

  if (typeof initial === 'function' 
        && typeof middleware === 'undefined') {
    middleware = initial
    initial = undefined
  }

  if (typeof middleware !== 'undefined') {
    return middleware(createStore)(reducers, initial)
  }

  return {
    dispatch(action) {
      for (let [key, reducer] of Object.entries(reducers)){
        state = reducer(state, action) || state;
      }
      for(let listener of listeners) {
        listener(state);
      }

      actions.push(action);

      return action;
    },
    getState() {
      return state;
    },
    subscribe(listener) {
      listeners.push(listener);
    },
    getActions () {
      return actions;
    },
  };
}

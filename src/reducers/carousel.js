export const carouselReducer = (state, action) => {
  switch (action.type) {
    case 'FETCH_BANNERS_REQUEST':
      return {
        ...state,
        config: {
          ...state.config,
          fetchingBanners: true,
        },
      };
    case 'FETCH_BANNERS_SUCCESS':
      return {
        ...state,
        config: {
          ...state.config,
          fetchingBanners: false,
        },
        carousel: action.payload.banners,
      };
    default:
      return {
        ...state,
      };
  }
};

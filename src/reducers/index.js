import { createStore } from '../lib/state';
import { combineReducers } from '../lib/combineReducers';

import thunk from '../lib/thunkMiddleware';
import applyMiddleware from '../lib/applyMiddleware';

import { configReducer } from './config';
import { carouselReducer } from './carousel';

const initialState = {
  config: {
    fetchingBanners: false,
    selectedBanner: 1,
  },
  carousel: [],
};

const reducers = { config: configReducer, carousel: carouselReducer };
const finalReducers = combineReducers(reducers);

export const rootReducer = createStore(finalReducers, initialState, applyMiddleware(thunk));

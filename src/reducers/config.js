export const configReducer = (state, action) => {
  switch (action.type) {
    case 'NEXT_BANNER':
      return {
        ...state,
        config: {
          ...state.config,
          selectedBanner: (state.config.selectedBanner >= state.carousel.length) ? 1 : state.config.selectedBanner + 1,
        },
      };
    case 'PREVIOUS_BANNER':
      return {
        ...state,
        config: {
          ...state.config,
          selectedBanner: (state.config.selectedBanner <= 1) ? state.carousel.length : state.config.selectedBanner - 1,
        },
      };
    case 'SELECT_BANNER':
      return {
        ...state,
        config: {
          ...state.config,
          selectedBanner: action.selectedBanner,
        },
      };
    default:
      return {
        ...state,
      };
  }
};

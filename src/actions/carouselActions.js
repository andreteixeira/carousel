import { fetchFromSeed } from '../lib/fetch';

export const fetchBannersRequest = () => {
  return {
    type: 'FETCH_BANNERS_REQUEST',
  };
};

export const fetchBannersSuccess = (banners) => {
  return {
    type: 'FETCH_BANNERS_SUCCESS',
    payload: { banners },
  };
};

export const fetchBannersFailure = (err) => {
  return {
    type: 'FETCH_BANNERS_FAILURE',
    payload: { err },
  };
};

export const fetchBanners = () => {
  return dispatch => {
    dispatch(fetchBannersRequest());
    return fetchFromSeed()
      .then(response => {
        dispatch(fetchBannersSuccess(JSON.parse(response)));
      }).catch(err => {
        dispatch(fetchBannersFailure(err));
      });
  };
};

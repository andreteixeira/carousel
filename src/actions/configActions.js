export const nextBanner = () => {
  return {
    type: 'NEXT_BANNER',
  };
};
export const previousBanner = () => {
  return {
    type: 'PREVIOUS_BANNER',
  };
};
export const selectBanner = (selectedBanner) => {
  return {
    type: 'SELECT_BANNER',
    selectedBanner: selectedBanner,
  };
};

import { render } from './view';
import { registerEventHandlers } from './events';
import { rootReducer } from './reducers';

import { fetchBanners } from './actions';

import './styles/style.scss';

rootReducer.dispatch(fetchBanners());

rootReducer.subscribe(newState => render(document.getElementById('container'), newState));
render(document.getElementById('container'), rootReducer.getState());

registerEventHandlers();

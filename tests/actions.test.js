import expect from 'expect';

import { rootReducer } from '../src/reducers';
import { fetchBanners } from '../src/actions';

describe('Async actions', () => {
  it('dispatches FETCH_BANNERS_SUCCESS when fetching banners has been done successfully', () => {

      const expectedActions = [
          { type: 'FETCH_BANNERS_REQUEST' },
          { 
              payload: {
                  banners: [{"id":1,"image_url":"http://res.cloudinary.com/dtg53rsic/image/upload/v1501001205/home/1.jpg", "title": "Destino 1", "price": 2222},
                            {"id":2,"image_url":"http://res.cloudinary.com/dtg53rsic/image/upload/v1501001205/home/2.jpg", "title": "Destino 2", "price": 1321},
                            {"id":3,"image_url":"http://res.cloudinary.com/dtg53rsic/image/upload/v1501001205/home/3.jpg", "title": "Destino 3", "price": 4213},
                            {"id":4,"image_url":"http://res.cloudinary.com/dtg53rsic/image/upload/v1501001205/home/4.jpg", "title": "Destino 4", "price": 3123},
                            {"id":5,"image_url":"http://res.cloudinary.com/dtg53rsic/image/upload/v1501001205/home/5.jpg", "title": "Destino 5", "price": 4125}] 
              },
              type: 'FETCH_BANNERS_SUCCESS'
          }
      ]

      return rootReducer.dispatch(fetchBanners('')).then(() => { 
      	expect(rootReducer.getActions()).toEqual(expectedActions)
      });
  });

});

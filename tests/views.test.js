import { render } from '../src/view';
import ReservationForm from '../src/view/components/reservationForm';
import Bullets from '../src/view/components/bullets';
import MobileMenu from '../src/view/components/mobileMenu';

import { rootReducer } from '../src/reducers';
import { fetchBanners } from '../src/actions';

beforeAll(() => {
  rootReducer.dispatch(fetchBanners());
});

describe('Views', () => {
  it('has a render method', () => {
      expect(render).toBeTruthy();
  });
});

describe('ReservationForm', () => {
  it('renders correctly', () => {
    expect(ReservationForm(rootReducer.getState())).toMatchSnapshot();
  });
}); 

describe('Bullets', () => {
  it('renders correctly', () => {
    expect(Bullets(rootReducer.getState())).toMatchSnapshot();
  });
}); 

describe('MobileMenu', () => {
  it('renders correctly', () => {
    expect(MobileMenu(rootReducer.getState())).toMatchSnapshot();
  });
});


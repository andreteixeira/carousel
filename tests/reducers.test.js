import expect from 'expect';

import { rootReducer } from '../src/reducers';
import { configReducer } from '../src/reducers/config';
import { nextBanner, previousBanner, fetchBanners, selectBanner } from '../src/actions';

const expectedInitialState = {"carousel": [{"id": 1, "image_url": "http://res.cloudinary.com/dtg53rsic/image/upload/v1501001205/home/1.jpg", "price": 2222, "title": "Destino 1"}, {"id": 2, "image_url": "http://res.cloudinary.com/dtg53rsic/image/upload/v1501001205/home/2.jpg", "price": 1321, "title": "Destino 2"}, {"id": 3, "image_url": "http://res.cloudinary.com/dtg53rsic/image/upload/v1501001205/home/3.jpg", "price": 4213, "title": "Destino 3"}, {"id": 4, "image_url": "http://res.cloudinary.com/dtg53rsic/image/upload/v1501001205/home/4.jpg", "price": 3123, "title": "Destino 4"}, {"id": 5, "image_url": "http://res.cloudinary.com/dtg53rsic/image/upload/v1501001205/home/5.jpg", "price": 4125, "title": "Destino 5"}], "config": {"fetchingBanners": false, "selectedBanner": 1}}

beforeAll(() => {
  rootReducer.dispatch(fetchBanners());
});

describe('rootReducer reducer', () => {
	it('exists', () => {
    expect(rootReducer).toBeTruthy();
	});
	it('returns the expected initial state', () => {
		expect(rootReducer.getState()).toEqual(expectedInitialState);
	});
	it('returns the expected combined reducers', () => {
		expect(Object.keys(rootReducer.getState())).toEqual([ 'config', 'carousel' ]);
	});
});

describe('Config reducer', () => {
	it('should increment config.selectedBanner on NEXT_BANNER', () => {
		const currBanner = rootReducer.getState().config.selectedBanner;
		const newBanner = configReducer(rootReducer.getState(), nextBanner()).config.selectedBanner;
    expect(newBanner).toBe(currBanner + 1);
	});
	it('should decrement config.selectedBanner on PREVIOUS_BANNER', () => {
		const currBanner = rootReducer.getState().config.selectedBanner;
		const newBanner = configReducer(rootReducer.getState(), previousBanner()).config.selectedBanner;
    	expect(newBanner).toBe(currBanner - 1 || rootReducer.getState().carousel.length);
	});
});

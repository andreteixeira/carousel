# KLM Carousel challenge
Some notes I made while producing my KLM Carousel code challenge.

## The notes

 - I tried to setup an app avoiding the usage of framework as suggested on the instructions. Nowadays I like very much the approach of React library for View rendering, as well as the unidirectional data flow proposed by Redux. On this activity I will use some of it's main patterns with Vanilla JS.

 - My first step was the setup of a very basic responsive header component with branding content on the left and account content to the right, it is rendered from index.js using component functions that replace root container innerHTML using ES6 template literals;

 - I used a lot of Flexbox to compose my screens, it is one of my favorite recently acquired knowledge. It makes things better aligned with less code and better organization.

 - I chose the class driven approach for my UI because of its convenience and modularity.

 - I chose Redux pattern unidirectional data flow approach because it makes simpler to manage state centralizing all the change actions and their storage; 

 - The 2 previous topics combined makes an efficient re-rendering when necessary 

 - I've setup webpack to bundle my /src content into a single js file;


## Requirements
```
**nodeJS** must not be greater than 7
```
## Install
```
  git clone https://andreteixeira@bitbucket.org/andreteixeira/carousel.git
  cd carousel
  yarn install
```
## Run the tests
```
  yarn run test
```
## Run the development server
```
  yarn start
```

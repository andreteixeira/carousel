const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const version = require('./package.json').version;

// Source map
const devtool = 'cheap-module-source-map';

// App entry
const entry =  [
    'babel-polyfill',
    'webpack-hot-middleware/client',
    './src/index.js'
]


// App js output
const output =  {
    path: path.join(__dirname, 'dist'),
    filename: "bundle.js",
    publicPath: '/dist/'
}


// Loaders
const loaders = [
    {
        loader: "babel-loader",
        test: /\.js$/,
        exclude: /node_modules/
    },{ 
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract({ fallback: 'style-loader', use: 'css-loader!sass-loader' })
    },{ 
        test: /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
        loader: 'file-loader'
    },{ 
        test: /\.(jpg|png?)(\?[a-z0-9]+)?$/,
        loader: 'file-loader'
    },{
        test: /\.(json)$/,
        exclude: /node_modules/,
        loader: 'json-loader',
    },{
        loader: "sass-loader",
        test: /\.scss$/,
        options: {
            includePaths: [path.resolve(__dirname, "./src/styles")]
        }
    }
]


// Dev plugins
const devPlugins =  [
    // Hot module
    new webpack.HotModuleReplacementPlugin(),
    // Avoid stopping the app execution on error
    new webpack.NoEmitOnErrorsPlugin(),
    // CSS Package
    new ExtractTextPlugin({
        filename: 'main.css',
        allChunks: true
    }),
]


module.exports = {
    devtool: devtool,
    entry: entry,
    output: output,
    module: {
        loaders: loaders
    },
    plugins: devPlugins
};
